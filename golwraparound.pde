// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// Daniel Shiffman, Nature of Code

// A basic implementation of John Conway's Game of Life CA
// how could this be improved to use object oriented programming?
// think of it as similar to our particle system, with a "cell" class
// to describe each individual cell and a "cellular automata" class
// to describe a collection of cells

// Cells wrap around

GOL gol;

void setup() {
  size(800, 300);
  gol = new GOL();
  frameRate(1);
}

void draw() {
  background(255);

  gol.generate();
  gol.display();
}

// reset board when mouse is pressed
void mousePressed() {
  //gol.init();
  int column = mouseX/100;
  int row = mouseY/100;
  gol.board[column][row] = 1;
  println(column+" "+row);
}